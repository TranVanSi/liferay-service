package vn.sdt.liferayuser.dto;

import lombok.Data;

@Data
public class RecordDTO {
    private Long recordId;
    private String name;
}
