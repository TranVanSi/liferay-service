package vn.sdt.liferayuser.util;


import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.StringPool;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.Hashtable;
/*@Component*/
public class LDAPConnection {

    private String serverIP = "10.196.133.116:389";
    private String security_principalLDAP = "ou=People,dc=dtt,dc=vn";
    private String security_credentialsLDAP = "secret";

    DirContext ldapContext;

    // connect to ldap
    public LDAPConnection() throws NamingException {

        if (serverIP == null) {
            serverIP = "10.196.133.116:389";
        }

        Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
        ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        ldapEnv.put(Context.PROVIDER_URL, "ldap://" + serverIP);
        ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        ldapEnv.put(Context.SECURITY_PRINCIPAL, security_principalLDAP);
        ldapEnv.put(Context.SECURITY_CREDENTIALS, security_credentialsLDAP);
        ldapContext = new InitialDirContext(ldapEnv);

    }

    public boolean createLDAPEntry(User user, String password) {
        boolean flag = false;
        long userId = user.getUserId();
        String cn = user.getFirstName();
        String sn = user.getLastName();
        String mail = user.getEmailAddress();
        String givenName = user.getFirstName() + (user.getMiddleName() != null && user.getMiddleName().length() > 0 ? (StringPool.SPACE + user.getMiddleName()) : StringPool.BLANK) + StringPool.SPACE + user.getLastName();

        String uid = "cd" + userId;

        Attribute userCn = new BasicAttribute("cn", cn);
        Attribute userSn = new BasicAttribute("sn", sn);

        String telephoneNumber = "+84"+user.getJobTitle().substring(1);
        Attribute userMail = new BasicAttribute("mail", mail);
        Attribute userSDT = new BasicAttribute("telephoneNumber", telephoneNumber);
        Attribute uidAttr = new BasicAttribute("uid", uid);
        Attribute userGivename = new BasicAttribute("givenName", givenName);
        Attribute userPassword = new BasicAttribute("userPassword", password);
        //ObjectClass attributes
        Attribute oc = new BasicAttribute("objectClass");
        oc.add("inetOrgPerson");

        // new entry properties
        Attributes entry = new BasicAttributes();
        entry.put(userCn);
        entry.put(userSn);
        entry.put(userMail);
        entry.put(userSDT);
        entry.put(userGivename);
        entry.put(uidAttr);
        entry.put(userPassword);
        entry.put(oc);
        //uid=cd + userId,ou=People,dc=dtt,dc=vn
        String entryDN = "mail=" + mail.trim() + ",ou=People,dc=dtt,dc=vn";
       
        try {
            ldapContext.createSubcontext(entryDN, entry);
            flag = true;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return flag;
        }
        return flag;

    }

    public boolean createLDAPEntryToChuc(User user, String password) {
        boolean flag = false;
        long userId = user.getUserId();
        String cn = user.getScreenName();
        String sn = user.getScreenName();
        String mail = user.getEmailAddress();
        String givenName = user.getScreenName();

        String uid = "tc" + userId;

        Attribute userCn = new BasicAttribute("cn", cn);
        Attribute userSn = new BasicAttribute("sn", sn);
        Attribute userMail = new BasicAttribute("mail", mail);
        Attribute uidAttr = new BasicAttribute("uid", uid);
        Attribute userGivename = new BasicAttribute("givenname", givenName);
        Attribute userPassword = new BasicAttribute("userPassword", password);
        //ObjectClass attributes
        Attribute oc = new BasicAttribute("objectClass");
        oc.add("inetOrgPerson");

        // new entry properties
        Attributes entry = new BasicAttributes();
        entry.put(userCn);
        entry.put(userSn);
        entry.put(userMail);
        entry.put(userGivename);
        entry.put(uidAttr);
        entry.put(userPassword);
        entry.put(oc);
        //uid=cd + userId,ou=People,dc=dtt,dc=vn
        String entryDN = "mail=" + mail.trim() + ",ou=People,dc=sdtech,dc=vn";

        try {
            ldapContext.createSubcontext(entryDN, entry);
            flag = true;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return flag;
        }
        return flag;

    }

    public boolean changePasswordLDAP(String password, String email) throws NamingException {
        boolean flag = true;
        try {
            SearchResult result = searchUserInLDAPByEmail(email);
            String uidSelected = result.getAttributes().get("uid").toString().trim();
            ModificationItem[] mods = new ModificationItem[1];
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute("userPassword", password));

            String uid = uidSelected;

            String ou = "ou=People,dc=dtt,dc=vn";

            ldapContext.modifyAttributes("mail=" + email.trim() + "," + ou, mods);

        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        ldapContext.close();
        return flag;
    }

    public SearchResult searchUserInLDAPByEmail(String emailAdress) throws NamingException {
        String searchBase = "OU=People,DC=dtt,DC=vn";
        String searchFilter = "(&(objectClass=person)"
                + "(mail=" + emailAdress + "))";
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        NamingEnumeration<SearchResult> answer = null;
        try {
            answer = ldapContext.search(searchBase, searchFilter, searchControls);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SearchResult searchResult = null;
        while (answer.hasMoreElements()) {
            searchResult = answer.nextElement();
        }
        return searchResult;
    }

    public boolean checkOldPassword(String oldPassword, String email) throws NamingException {
        boolean correct = false;
        DirContext ldapContextCheckPass;

        String security_credentials = oldPassword;
        SearchResult result = searchUserInLDAPByEmail(email);
        String uidSelected = result.getAttributes().get("uid").toString().trim();

        String uidValue = uidSelected;
        try {
            Hashtable ldapEnv = new Hashtable(11);
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL, "ldap://" + serverIP);
            ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
            ldapEnv.put(Context.SECURITY_PRINCIPAL, "mail=" + email.trim() + ",OU=People,DC=dtt,DC=vn");
            ldapEnv.put(Context.SECURITY_CREDENTIALS, security_credentials);
            ldapContextCheckPass = new InitialDirContext(ldapEnv);
            correct = true;
        } catch (Exception e) {

            e.printStackTrace();
        }
        return correct;
    }

    public byte[] encodePassword(String password) {
        String quotedPassword = "\"" + password + "\"";
        char unicodePwd[] = quotedPassword.toCharArray();
        byte pwdArray[] = new byte[unicodePwd.length * 2];
        for (int i = 0; i < unicodePwd.length; i++) {
            pwdArray[i * 2 + 1] = (byte) (unicodePwd[i] >>> 8);
            pwdArray[i * 2 + 0] = (byte) (unicodePwd[i] & 0xff);
        }
        return pwdArray;
    }

    /*
     * Dành cho các laoij đối tượng
     * */
    public boolean createLDAPEntryWithDoiTuong(User user, String password, long loaiDoiTuongId) {
        boolean flag = false;
        long userId = user.getUserId();
        String cn = user.getScreenName();
        String sn = user.getScreenName();
        String mail = user.getEmailAddress();
        String givenName = user.getScreenName();
        /*String uid ="uid=" + "cc" + userId;*/
        String uid = "";
        if (loaiDoiTuongId == Constants.LoaiDoiTuong.CONGDAN) {
            uid = "cd" + userId;
        } else if (loaiDoiTuongId == Constants.LoaiDoiTuong.DOANHNGHIEP) {
            uid = "dn" + userId;
        } else if (loaiDoiTuongId == Constants.LoaiDoiTuong.TOCHUC) {
            uid = "tc" + userId;
        }

        Attribute userCn = new BasicAttribute("cn", cn);
        Attribute userSn = new BasicAttribute("sn", sn);
        Attribute userMail = new BasicAttribute("mail", mail);
        Attribute uidAttr = new BasicAttribute("uid", uid);
        Attribute userGivename = new BasicAttribute("givenname", givenName);
        Attribute userPassword = new BasicAttribute("userPassword", password);
        //ObjectClass attributes
        Attribute oc = new BasicAttribute("objectClass");
        oc.add("inetOrgPerson");

        // new entry properties
        Attributes entry = new BasicAttributes();
        entry.put(userCn);
        entry.put(userSn);
        entry.put(userMail);
        entry.put(userGivename);
        entry.put(uidAttr);
        entry.put(userPassword);
        entry.put(oc);
        //uid=cd + userId,ou=People,dc=dtt,dc=vn
        String entryDN = "mail=" + mail.trim() + ",ou=People,dc=sdtech,dc=vn";

        try {
            ldapContext.createSubcontext(entryDN, entry);
            flag = true;
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return flag;
        }
        return flag;

    }

}