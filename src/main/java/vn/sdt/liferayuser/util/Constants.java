package vn.sdt.liferayuser.util;

public class Constants {
    public interface LoaiDoiTuong {
        public final int CONGDAN = 1;
        public final int DOANHNGHIEP = 3;
        public final int TOCHUC = 7;
    }
}
