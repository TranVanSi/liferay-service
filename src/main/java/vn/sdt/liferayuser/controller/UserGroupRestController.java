package vn.sdt.liferayuser.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import vn.sdt.liferayuser.service.UserGroupService;

import java.util.Arrays;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/usergroup")
public class UserGroupRestController {

    private final UserGroupService userGroupService;

    @PostMapping("/addChucVuToCanBo")
    private boolean ganChucVuToCanBo(@RequestParam(value = "userId", defaultValue = "0") long userId,
                                     @RequestParam(value = "maChucVu", defaultValue = "") String maChucVu,
                                     @RequestParam(value = "maCoQuan", defaultValue = "") String maCoQuan) throws Exception {

        return userGroupService.ganChucVuToCanBo(userId, maCoQuan, maChucVu);
    }

    @PostMapping("/addUserGroup")
    private void addUserGroup(@RequestParam(value = "userId", defaultValue = "0") long userId,
                              @RequestParam(value = "maChucVu", defaultValue = "") String maChucVu,
                              @RequestParam(value = "maCoQuan", defaultValue = "") String maCoQuan,
                              @RequestParam(value = "roleIds", defaultValue = "") String roleIdsStr) throws Exception {

        String[] arr = roleIdsStr.split(",");
        long[] roleIds = Arrays.stream(arr).mapToLong((item) -> ((long) Long.parseLong(item))).toArray();
        userGroupService.addUserGroup(userId, maCoQuan, maChucVu, roleIds);
    }

    @GetMapping("/getGroups")
    private String getGroups(@RequestParam(value = "companyId", defaultValue = "0") long companyId,
                             @RequestParam(value = "parentGroupId", defaultValue = "0") long parentGroupId,
                             @RequestParam(value = "site", defaultValue = "false") boolean site) {

        return userGroupService.getGroups(companyId, parentGroupId, site);
    }
}
