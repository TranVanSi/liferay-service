package vn.sdt.liferayuser.controller;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.sdt.liferayuser.service.LayoutService;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/layout")
public class LayoutRestController {

    private final LayoutService layoutService;

    @GetMapping("/{groupId}/{privateLayout}")
    public String getRolesByGroupId(@PathVariable(value = "groupId") long groupId,
                                    @PathVariable(value = "privateLayout") boolean privateLayout ){

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("layouts", layoutService.getLayouts(groupId, privateLayout));

        return jsonObject.toString();
    }
}
