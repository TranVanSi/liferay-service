package vn.sdt.liferayuser.controller;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;
import vn.sdt.liferayuser.service.RoleService;

import java.net.URLDecoder;
import java.util.Date;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/role")
public class RoleRestController {

    private final RoleService roleService;

    @GetMapping("/{roleId}")
    public String fetchRole(@PathVariable long roleId) {

        return RoleLocalServiceUtil.fetchRole(roleId).getName();
    }

    @PostMapping("/create")
    public long addRole(@RequestParam(value = "name", defaultValue = "") String roleName,
                        @RequestParam(value = "companyId", defaultValue = "0") long companyId,
                        @RequestParam(value = "userId", defaultValue = "0") long userId,
                        @RequestParam(value = "type", defaultValue = "1") int type) {

        roleName = URLDecoder.decode(roleName);

        Role role = RoleLocalServiceUtil.createRole(CounterLocalServiceUtil
                .increment(Role.class.getName()));

        role.setName(roleName);
        role.setCompanyId(companyId);
        role.setUserId(userId);
        role.setType(type);
        role.setClassName(Role.class.getName());
        role.setClassPK(role.getRoleId());
        role.setCreateDate(new Date());
        role.setModifiedDate(new Date());

        RoleLocalServiceUtil.updateRole(role);

        return role.getRoleId();
    }

    @PutMapping("/update")
    public long updateRole(@RequestParam(value = "name", defaultValue = "") String roleName,
                        @RequestParam(value = "roleId", defaultValue = "0") long roleId) {

        roleName = URLDecoder.decode(roleName);
        Role role = RoleLocalServiceUtil.fetchRole(roleId);
        if (Validator.isNotNull(role)) {
            role.setName(roleName);
            RoleLocalServiceUtil.updateRole(role);
        }

        return roleId;
    }

    @DeleteMapping("/delete")
    public void deleteRole(@RequestParam(value = "roleId", defaultValue = "0") long roleId) throws Exception {
        RoleLocalServiceUtil.deleteRole(roleId);
    }

    @GetMapping("/{userId}/{groupId}")
    public String getRolesByUserId(@PathVariable(value = "userId") long userId,
                                          @PathVariable(value = "groupId") long groupId ) throws Exception{

        User user = UserLocalServiceUtil.fetchUser(userId);
        JSONObject jsonObject = new JSONObject();
        boolean isAdmin = PortalUtil.isGroupAdmin(user, groupId);

        jsonObject.put("isAdmin", isAdmin);
        jsonObject.put("roles", roleService.findAllRolesForUser(user));

        return jsonObject.toString();
    }
}
