package vn.sdt.liferayuser.controller;

import lombok.RequiredArgsConstructor;
import vn.sdt.liferayuser.model.Record;
import vn.sdt.liferayuser.service.RecordService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SimpleController {

    private final RecordService recordService;

    @GetMapping("/echo")
    public Iterable<Record> echo() {
        return recordService.getRecords();
    }

}
