package vn.sdt.liferayuser.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import vn.sdt.liferayuser.dto.UserDTO;
import vn.sdt.liferayuser.service.UserService;

import javax.naming.NamingException;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/user")
public class UserRestController {

    private final UserService userService;

    @GetMapping("/{userId}")
    public UserDTO fetchUser(@PathVariable long userId) {

        return userService.findByUserId(userId);
    }

    @PostMapping(value = "/create")
    public long addUser(@RequestBody UserDTO user) throws Exception {

        return userService.addUser(user);
    }

    @PutMapping("/update")
    public void updateUser(@RequestBody UserDTO user) {
        userService.updateUser(user);
    }

    @GetMapping("/getUserIdByEmail")
    public long findByEmail(@RequestParam(value = "companyId", defaultValue = "0") long companyId,
                            @RequestParam(value = "email", defaultValue = "") String email) throws Exception {

        return userService.getUserIdByEmail(companyId, email);
    }

    @PutMapping("/changePassword")
    public void changePassword(@RequestParam(value = "password", defaultValue = "") String password,
                               @RequestParam(value = "email", defaultValue = "") String email) throws NamingException {

        userService.changePasswordLDAP(password, email);
    }

    @GetMapping("/checkOldPassword")
    public boolean checkOldPassword(@RequestParam(value = "password", defaultValue = "") String password,
                                    @RequestParam(value = "email", defaultValue = "") String email) throws NamingException {

        return userService.checkOldPassword(password, email);
    }
}
