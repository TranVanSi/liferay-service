package vn.sdt.liferayuser.mapper;

import vn.sdt.liferayuser.dto.RecordDTO;
import vn.sdt.liferayuser.model.Record;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RecordMapper {
    Record recordDTOToRecord(RecordDTO recordDTO);
}