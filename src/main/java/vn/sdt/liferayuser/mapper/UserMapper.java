package vn.sdt.liferayuser.mapper;

import com.liferay.portal.kernel.model.User;
import org.mapstruct.Mapper;
import vn.sdt.liferayuser.dto.UserDTO;

@Mapper(componentModel = "spring")
public interface UserMapper{

   UserDTO userToUserDto(User user);
}