package vn.sdt.liferayuser.service;

import vn.sdt.liferayuser.dao.RecordsRepository;
import vn.sdt.liferayuser.dto.RecordDTO;
import vn.sdt.liferayuser.mapper.RecordMapper;
import vn.sdt.liferayuser.model.Record;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RecordService {

    private final RecordsRepository recordsRepository;
    private final RecordMapper recordMapper;

    public Iterable<Record> getRecords() {
        return recordsRepository.findAll();
    }

    public void addRecord(RecordDTO recordDTO) {
        Record record = recordMapper.recordDTOToRecord(recordDTO);
        recordsRepository.save(record);
    }
}
