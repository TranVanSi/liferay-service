package vn.sdt.liferayuser.service;

import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RoleService {

    public static List<Role> findAllRolesForUser(User user){

        List<Role> roles = new ArrayList<Role>();

        roles.addAll(RoleLocalServiceUtil.getUserRoles(user.getUserId()));
        roles.addAll(RoleLocalServiceUtil.getUserRelatedRoles(user.getUserId(), user.getGroupIds()));

        return roles;
    }
}
