package vn.sdt.liferayuser.service;

import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.OrganizationLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.sdt.liferayuser.dto.UserDTO;
import vn.sdt.liferayuser.mapper.UserMapper;
import vn.sdt.liferayuser.util.LDAPConnection;

import javax.naming.NamingException;
import java.util.Calendar;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class UserService {

    private final UserMapper userMapper;
    private final UserGroupService userGroupService;


    public UserDTO findByUserId(long userId) {

        return userMapper.userToUserDto(UserLocalServiceUtil.fetchUser(userId));
    }

    public long addUser(UserDTO userDTO) throws Exception {
        // Set the birth day as current date
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int birthdayMonth = cal.get(Calendar.MONTH);
        int birthdayDay = cal.get(Calendar.DAY_OF_MONTH);
        int birthdayYear = cal.get(Calendar.DAY_OF_MONTH);
        long[] organizationIds = null;
        //for set organization
        try {
            if (userDTO.getOrganizationName().length() > 0) {
                String orgName = userDTO.getOrganizationName();

                Organization organization = OrganizationLocalServiceUtil.getOrganization(userDTO.getCompanyId(), orgName);

                organizationIds = new long[]{organization.getOrganizationId()};
            }

            User user = UserLocalServiceUtil.addUser(userDTO.getCreatorUserId(), userDTO.getCompanyId(), false, userDTO.getPassword(), userDTO.getPassword(),
                    true, userDTO.getScreenName(), userDTO.getEmailAddress(), 0, userDTO.getOpenId(),
                    userDTO.getLocale(), userDTO.getFirstName(), userDTO.getMiddleName(), userDTO.getLastName(), 0, 0, true,
                    birthdayMonth, birthdayDay, birthdayYear, userDTO.getJobTitle(), null,
                    organizationIds, null, null, false, new ServiceContext());


            // If have LDAP
       /* if (user != null) {
            LDAPConnection ldapContext = new LDAPConnection();
            ldapContext.createLDAPEntry(user, userDTO.getPassword());
        }*/

            return user.getUserId();
        }catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }

    public void updateUser(UserDTO userDTO) {
        User user = UserLocalServiceUtil.fetchUser(userDTO.getUserId());
        user.setFirstName(userDTO.getFirstName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setLastName(userDTO.getLastName());

        UserLocalServiceUtil.updateUser(user);
    }

    public long getUserIdByEmail(long companyId, String email) {

        try {
            return UserLocalServiceUtil.getUserIdByEmailAddress(companyId, email);
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean changePasswordLDAP (String password, String email) throws NamingException {
        LDAPConnection ldapContext = new LDAPConnection();

        return ldapContext.changePasswordLDAP(password, email);
    }

    public boolean checkOldPassword (String password, String email) throws NamingException {
        LDAPConnection ldapContext = new LDAPConnection();

        return ldapContext.checkOldPassword(password, email);
    }
}
