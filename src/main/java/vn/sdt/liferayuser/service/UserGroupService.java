package vn.sdt.liferayuser.service;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.Validator;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class UserGroupService {

    public boolean ganChucVuToCanBo(long userId, String maCoQuan, String maChucVu) throws Exception{
        User user = UserLocalServiceUtil.getUserById(userId);

        long companyId = user.getCompanyId();
        String userGroupName = getUserGroupName(maChucVu, maCoQuan);
        String userGroupNameSub = getUserGroupName(maCoQuan, maChucVu);

        UserGroup userGroup = findUserGroupByName(companyId, userGroupName);

        if (userGroup == null){
            userGroup = findUserGroupByName(companyId, userGroupNameSub);
        }

        if (userGroup != null) {

            return addUserToUserGroup(userGroup.getGroup().getGroupId(), userGroup.getUserGroupId(), user.getUserId());

        }

        return false;
    }

    private UserGroup findUserGroupByName(long companyId, String name) {

        List<UserGroup> results = UserGroupLocalServiceUtil.getUserGroups(companyId);
        for (UserGroup userGroup : results) {
            if (userGroup.getName().trim().toLowerCase().equals(name.trim().toLowerCase())) {
                return userGroup;
            }
        }

        return null;
    }

    private String getUserGroupName(String tenCoQuan, String chucVu) {

        String ten = tenCoQuan + "_" + chucVu;
        ten = ten.replace("?", "");
        ten = ten.replace(",", "");

        return ten;

    }

    public boolean addUserToUserGroup(long groupId, long userGroupId, long userId) throws Exception{
        long[] userIds = {userId};

        UserLocalServiceUtil.addGroupUsers(groupId, userIds);
        UserLocalServiceUtil.addUserGroupUsers(userGroupId, userIds);

        return true;
    }

    public void removeUserFromUserGroup(long companyId, long groupId, long userGroupId, long userId) throws Exception{
        long[] groupIds = {groupId};
        long[] userIds = {userId};

        UserLocalServiceUtil.unsetUserGroupUsers(userGroupId, userIds);

        GroupLocalServiceUtil.unsetUserGroups(userId, groupIds);
    }

    public void addUserGroup (long userId, String maCoQuan, String maChucVu, long[] roleIds) throws Exception {

        User user = UserLocalServiceUtil.fetchUser(userId);
        String tenGroup = getUserGroupName(maCoQuan, maChucVu);
        // Tim xem da tao Group voi ten nhu tren chua
        UserGroup userGroup = findUserGroupByName(user.getCompanyId(), tenGroup);
        long groupIdS[] = null;

        // Neu tao roi thi xoa toan bo cac lien ket di
        if (userGroup != null) {

            groupIdS = new long[]{userGroup.getGroup().getGroupId()};

            /*
             *  Xoa toan bo role ep den group truoc day
             *  b1. Lay toan bo Vatro theo chuc vu.
             *  b2. Lay toan bo tai nguye theo vai tro.
             *  b3. unset role vao group
             */

            //delete all role assign for Group
            List<Role> rList = RoleLocalServiceUtil.getRoles(user.getCompanyId());
            String roleIdStr = "";
            if(rList != null){
                RoleLocalServiceUtil.deleteGroupRoles(groupIdS[0], rList);
            }
        } else {
            //create new group

            userGroup = UserGroupLocalServiceUtil.addUserGroup(user.getUserId(), user.getCompanyId(), tenGroup, "", null);
        }

        //insert role vao group
        RoleLocalServiceUtil.addGroupRoles(userGroup.getGroupId(), roleIds);

    }

    public String getGroups (long companyId, long parentGroupId, boolean site) {

        JSONArray jsonArray = new JSONArray();
        List<Group> groups = GroupLocalServiceUtil.getGroups(companyId, parentGroupId, site);

        if (Validator.isNotNull(groups)) {
            for (Group group:groups) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("groupId", group.getGroupId());
                jsonObject.put("groupName", group.getGroupKey());

                jsonArray.put(jsonObject);
            }
        }

        return jsonArray.toString();
    }
}
