package vn.sdt.liferayuser.service;

import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class LayoutService {

    public List<String> getLayouts (long groupId, boolean privateLayout) {
        List<String> layouts = new ArrayList<>();
        List<Layout> rootLayouts = LayoutLocalServiceUtil.getLayouts(groupId, privateLayout);

        for (Layout layout : rootLayouts) {
            layouts.add(layout.getFriendlyURL());

            if (layout.getAllChildren() != null) {
                layouts.addAll(layout.getAllChildren().stream().map(Layout -> Layout.getFriendlyURL()).collect(Collectors.toList()));
            }
        }

        return layouts;
    }
}
