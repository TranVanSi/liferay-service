package vn.sdt.liferayuser.dao;

import vn.sdt.liferayuser.model.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordsRepository extends CrudRepository<Record, Long> {

}
